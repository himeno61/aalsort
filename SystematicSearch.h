//
// Created by luke on 28.05.18.
//

#ifndef CMYKSORT_SYSTEMATICSEARCH_H
#define CMYKSORT_SYSTEMATICSEARCH_H

#include <list>
#include <iterator>
#include <ostream>

class SystematicSearch {
    std::list<std::string> listOfStrings;
public:
    virtual ~SystematicSearch();

     explicit SystematicSearch(std::string text);


    void printList();
    void sort();
    bool isSorted(std::string);
    int addAllPossibleSettings(std::string string);
    bool contains(std::string element);
    std::string moveFour(int pos,std::string string);\
    std::string returnLast();
};


#endif //CMYKSORT_SYSTEMATICSEARCH_H
