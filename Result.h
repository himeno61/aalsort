//
// Created by luke on 02.06.18.
//

#ifndef CMYKSORT_RESULT_H
#define CMYKSORT_RESULT_H


#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
struct singleResult{
    int position;
    double time;
};

class Result {
    std::vector<singleResult> vector;
public:
    Result() {}
    virtual ~Result() {vector.clear();vector.shrink_to_fit();}
    void add(int position, double time);
    std::stringstream returnStream();
};
#endif //CMYKSORT_RESULT_H
