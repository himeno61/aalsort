//
// Created by luke on 18.05.18.
//

#ifndef CMYKSORT_GRUPA_H
#define CMYKSORT_GRUPA_H

#include <string>
#include <ostream>
#include <exception>
#include <stdexcept>
#include <exception>
#include "erros.h"
#include "SystematicSearch.h"

class StringSearch {
private:
    std::string tekst;
    int numberOfSorted;

public:
    explicit StringSearch(const std::string &tekst);

    friend std::ostream &operator<<(std::ostream &os, const StringSearch &grupa1);

public:
    void sort();
    void sortWithSystematicSearch();
    void setLetter(char c);
    void setLetter0(int pos);
    void setLetter1(int pos);
    void setLetter2(int pos);
    void setLetter3(int pos);
    void deleteFours();
    void deleteFours(int startingPos);
    int countRelativeIndex(int index);
    void move(int pos);

    virtual ~StringSearch();
};


#endif //CMYKSORT_GRUPA_H
