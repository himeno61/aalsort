//
// Created by luke on 25.05.18.
//

#ifndef CMYKSORT_GENERATOR_H
#define CMYKSORT_GENERATOR_H


#include <string>
#include <iostream>

class generator {
public:
    std::string generate(int size) {
        std::string text;
        for (int i = 0; i < size; ++i) {
            text.append("cmyk");
        }
        srand (time(nullptr));
        for (int i = 0; i < 4*text.size(); ++i) {
            unsigned long pos = rand()%(text.size()-6) ;
            std::string temp = text.substr(pos, 4);
            text.erase(pos, 4);
            text.append(temp);
        }
        return text;
    }
};


#endif //CMYKSORT_GENERATOR_H
