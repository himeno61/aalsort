//
// Created by luke on 04.06.18.int
//

#include <iostream>
#include "SubstringSearch.h"



SubstringSearch::SubstringSearch(const std::string &text) : text(text),numberOfSorted(0) {}

SubstringSearch::~SubstringSearch() = default;

std::ostream &operator<<(std::ostream &os, const SubstringSearch &search) {
    os << "text: " << search.text;
    return os;
}



std::pair<int,int> SubstringSearch::findLongestSubstring(char c) {
    int indexTemp = text.find(c, numberOfSorted);
    if (indexTemp > text.size())
        throw NoLettersLeft();

    int lengthTemp = 1;
    int lengthMax = 1;
    int indexMax = indexTemp;

    char currentLetterTable[4] = {'c', 'm', 'y', 'k'};
    int charIndex = countLetter(c);

    int index = indexTemp;
    do {
        if (text.at(index + 1) == currentLetterTable[(charIndex + 1) % 4]) {
            lengthTemp++;
            charIndex = (charIndex+1)%4;
            index++;
        }
        else {
            if (lengthTemp > lengthMax) {
                lengthMax = lengthTemp;
                indexMax = indexTemp;
            }
            indexTemp = text.find(c, index);
            if (indexTemp > text.size())
                break;
            lengthTemp = 1;
            index = indexTemp ;
            charIndex = ((countLetter(c))%4);
        }
    } while (index<text.size()-1);
    return std::make_pair(indexMax,lengthMax);
}

int SubstringSearch::countLetter(char c) {
    switch (c){
        case 'c':
            return 0;
        case 'm':
            return 1;
        case 'y':
            return 2;
        case 'k':
            return 3;
    }
}
