//
// Created by luke on 27.05.18.
//

#ifndef CMYKSORT_FILEREADER_H
#define CMYKSORT_FILEREADER_H

#include <iostream>
#include <fstream>
#include <utility>

class fileReader{
    std::string path;
public:
    explicit fileReader(std::string path) : path(std::move(path)) {}

    virtual ~fileReader() = default;;

    std::string readFile(){
        std::string text;
        std::string line;
        std::ifstream myfile (path);
        if (myfile.is_open())
        {
            while ( getline (myfile,line) )
            {
                text.append(line);
            }
            myfile.close();
        }
        return text;
    }

};



#endif //CMYKSORT_FILEREADER_H
