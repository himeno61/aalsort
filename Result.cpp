//
// Created by luke on 02.06.18.
//


#include "Result.h"


void Result::add(int position, double time) {
    struct singleResult single;
    single.position = position;
    single.time = time;
    vector.push_back(single);
}

std::stringstream Result::returnStream() {
    std::stringstream ss;
    ss<<"\n\n\n";
    ss<<std::setw(25)<<"RESULT TABLE\n";
    ss<<std::setw(15)<<"Population size"<<std::setw(15)<<"Time\n";
    for (auto &i : vector)
        ss<<std::setw(15)<< i.position<<std::setw(15)<< i.time<<std::endl;
    return  ss;
}
