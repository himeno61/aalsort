//
// Created by luke on 20.05.18.
//

#ifndef CMYKSORT_ERROS_H
#define CMYKSORT_ERROS_H

struct NoLettersLeft : public std::exception
{
    const char * what () const noexcept {
        return "No more letter";
    }
};

struct SetFound : public std::exception
{
    const char * what () const noexcept {
        return "Set found";
    }
};


#endif //CMYKSORT_ERROS_H
