//
// Created by luke on 28.05.18.
//

#include <iostream>
#include <algorithm>
#include "SystematicSearch.h"
#include "erros.h"

SystematicSearch::SystematicSearch(std::string text) {listOfStrings.push_back(text);}

SystematicSearch::~SystematicSearch() {
    listOfStrings.clear();
}

void SystematicSearch::sort() {
    auto numberToAdvance=1;
    auto counter = 0;
    bool flag = true;
    auto it = listOfStrings.begin();
    do {
        for (int i = 0; i <numberToAdvance ; ++i,it++) {
            try {
                counter += addAllPossibleSettings(*it);
            }
            catch (SetFound setFound){
                flag = false;
                break;
            }
        }
        numberToAdvance = counter;
        counter = 0;
    }while (flag);
}

bool SystematicSearch::isSorted(std::string string) {
    char currentLetter [4] = {'c','m','y','k'};
    for (int i = 0; i <string.size() ; ++i) {
        if(string.at(i)!=currentLetter[i%4])
            return false;
    }
    return true;
}

int SystematicSearch::addAllPossibleSettings(std::string string) {
    int i;
    for (i = 0; i <string.size()-4 ; ++i) {
        std::string temp = moveFour(i,string);
        if(!contains(temp)) {
            listOfStrings.push_back(temp);
        }
        if(isSorted(temp)) {
            throw SetFound();
        }
    }
    return i+1;
}

bool SystematicSearch::contains( std::string element) {
    auto it = std::find(listOfStrings.begin(), listOfStrings.end(), element);
    return it != listOfStrings.end();
}

std::string SystematicSearch::moveFour(int pos, std::string string) {
    std::string temp = string.substr(pos, 4);
    string.erase(pos, 4);
    string.append(temp);
    return string;
}


void SystematicSearch::printList(){
    for (auto &listOfString : listOfStrings)
        std::cout << listOfString << std::endl;
}
std::string SystematicSearch::returnLast() {
    return listOfStrings.back();
}



