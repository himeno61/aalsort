//
// Created by luke on 04.06.18.
//

#ifndef CMYKSORT_SUBSTRINGSEARCH_H
#define CMYKSORT_SUBSTRINGSEARCH_H


#include <string>
#include <ostream>
#include "erros.h"

class SubstringSearch {
private:
    std::string text;
    int numberOfSorted;
    int countLetter(char c);
public:
    explicit SubstringSearch(const std::string &text);

    virtual ~SubstringSearch();

    std::pair<int,int> findLongestSubstring(char startChar);
    friend std::ostream &operator<<(std::ostream &os, const SubstringSearch &search);

};


#endif //CMYKSORT_SUBSTRINGSEARCH_H
