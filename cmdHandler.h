//
// Created by luke on 07.06.18.
//

#ifndef CMYKSORT_CMDHANDLER_H
#define CMYKSORT_CMDHANDLER_H
#include <iostream>
#include <cstring>
#include <chrono>
#include <fstream>
#include <string>
#include <algorithm>
#include "StringSearch.h"
#include "generator.h"
#include "fileReader.h"
#include "Result.h"
#include "SystematicSearch.h"
#include "SubstringSearch.h"

void helpMenu(){
    std::string line;
    std::string code = setlocale(LC_CTYPE, "");
    std::ifstream myfile;
    if(code == "pl_PL.UTF-8")
        myfile.open ("../resources/helpmePL");
    else
        myfile.open("../resources/helpmeENG");
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {
            std::cout << line << '\n';
        }
        myfile.close();
    }

    else std::cout << "Unable to open file";
}

void cmdArgumentsHandler(int argc,char * argv[]){
    if(argc==2 && (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0 )){
        helpMenu();
    }
    else if(strcmp(argv[1], "-m1") == 0 &&
            (strcmp(argv[2], "-g") == 0 )&&argc==4){
        generator* generator1= new generator();
        std::string text = generator1->generate(atoi(argv[3]));
        std::cout<<"input: "<<text<<std::endl;
        StringSearch *grupa1 = new StringSearch(text);
        grupa1->sort();
        std::cout <<*grupa1<<std::endl;
    } else if(strcmp(argv[1], "-m1") == 0 &&
              (strcmp(argv[2], "-f") == 0 )&& argc==4){
        std::string text;
        fileReader *input = new fileReader(argv[3]);
        text = input->readFile();
        StringSearch *grupa1 = new StringSearch(text);
        grupa1->sort();
        std::cout <<*grupa1<<std::endl;
    }
    else if((strcmp(argv[1], "-m1") ==0) &&  (argc==10) &&
            (strcmp(argv[2], "-n") == 0 )&& (strcmp(argv[4], "-k") == 0) &&
            (strcmp(argv[6], "-step") == 0) && (strcmp(argv[8], "-r") == 0)){
        generator* generator1= new generator();
        std::string text;
        StringSearch *grupa1;
        Result* result = new Result();
        std::chrono::high_resolution_clock::time_point t1,t2;
        std::vector <double> vector;
        for (int i = 0; i < atoi(argv[5]); ++i) {
            vector.clear();
            for (int j = 0; j < atoi(argv[9]); ++j) {
                std::cout<<"size: "<<atoi(argv[3]) + i*atoi(argv[7])<<" instance: "<<j<<std::endl;
                text = generator1->generate(atoi(argv[3]) + i*atoi(argv[7]));
                std::cout<<"before: "<<text<<std::endl;
                grupa1 = new StringSearch(text);
                t1 = std::chrono::high_resolution_clock::now();
                grupa1->sort();
                t2 = std::chrono::high_resolution_clock::now();
                vector.push_back ((double)(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000);
                std::cout<<*grupa1<<std::endl;
            }
            result->add(atoi(argv[3]) + i*atoi(argv[7]),*std::max_element(vector.begin(), vector.end()));
        }
        std::cout<<result->returnStream().str();
    }
    else if((strcmp(argv[1], "-m1") ==0) &&  (argc==6) &&
            (strcmp(argv[2], "-n") == 0 ) && (strcmp(argv[4], "-r") == 0)){
        generator* generator1= new generator();
        std::chrono::high_resolution_clock::time_point t1,t2;
        Result* result = new Result();
        StringSearch *grupa1;
        std::string text;
        for (int i = 0; i < atoi(argv[5]); ++i) {
            text = generator1->generate(atoi(argv[3]));
            grupa1 = new StringSearch(text);
            t1 = std::chrono::high_resolution_clock::now();
            grupa1->sort();
            t2 = std::chrono::high_resolution_clock::now();
            result->add(atoi(argv[3]),(double)(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000);
        }
        std::cout<<result->returnStream().str();
    }
    else if(strcmp(argv[1], "-m2") == 0 &&
            (strcmp(argv[2], "-g") == 0 )&&argc==4){
        generator* generator1= new generator();
        std::string text = generator1->generate(atoi(argv[3]));
        std::cout<<"input: "<<text<<std::endl;
        StringSearch *grupa1 = new StringSearch(text);
        grupa1->sortWithSystematicSearch();
        std::cout <<*grupa1<<std::endl;
    } else if(strcmp(argv[1], "-m2") == 0 &&
              (strcmp(argv[2], "-f") == 0 )&& argc==4){
        std::string text;
        fileReader *input = new fileReader(argv[3]);
        text = input->readFile();
        StringSearch *grupa1 = new StringSearch(text);
        grupa1->sortWithSystematicSearch();
        std::cout <<*grupa1<<std::endl;
    }
    else if((strcmp(argv[1], "-m2") ==0) &&  (argc==10) &&
            (strcmp(argv[2], "-n") == 0 )&& (strcmp(argv[4], "-k") == 0) &&
            (strcmp(argv[6], "-step") == 0) && (strcmp(argv[8], "-r") == 0)){
        generator* generator1= new generator();
        std::chrono::high_resolution_clock::time_point t1,t2;
        std::chrono::duration<int64_t, std::milli> temp;
        Result* result = new Result();
        std::string text;
        StringSearch *grupa1;
        std::vector <double> vector;
        for (int i = 0; i < atoi(argv[5]); ++i) {
            vector.clear();
            for (int j = 0; j < atoi(argv[9]); ++j) {
                std::cout<<"size: "<<atoi(argv[3]) + i*atoi(argv[7])<<" instance: "<<j<<std::endl;
                text = generator1->generate(atoi(argv[3]) + i*atoi(argv[7]));
                std::cout<<"before: "<<text<<std::endl;
                grupa1 = new StringSearch(text);
                t1 = std::chrono::high_resolution_clock::now();
                grupa1->sortWithSystematicSearch();
                t2 = std::chrono::high_resolution_clock::now();
                vector.push_back ((double)(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000);
                std::cout<<*grupa1<<std::endl;
                std::cout<<"time: "<<(double)(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000<<std::endl;
            }
            result->add(atoi(argv[3]) + i*atoi(argv[7]),*std::max_element(vector.begin(), vector.end()));
            std::cout<<"max: "<<*std::max_element(vector.begin(), vector.end())<<std::endl;
        }
        std::cout<<result->returnStream().str();
    }
    else if((strcmp(argv[1], "-m2") ==0) &&  (argc==6) &&
            (strcmp(argv[2], "-n") == 0 ) && (strcmp(argv[4], "-r") == 0)){
        generator* generator1= new generator();
        std::chrono::high_resolution_clock::time_point t1,t2;
        std::chrono::duration<int64_t, std::milli> temp;
        Result* result = new Result();
        StringSearch *grupa1;
        std::string text;
        for (int i = 0; i < atoi(argv[5]); ++i) {
            text = generator1->generate(atoi(argv[3]));
            grupa1 = new StringSearch(text);
            t1 = std::chrono::high_resolution_clock::now();
            grupa1->sortWithSystematicSearch();
            t2 = std::chrono::high_resolution_clock::now();
            result->add(atoi(argv[3]),(double)(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000);
        }
        std::cout<<result->returnStream().str();
    }

    else if(strcmp(argv[1], "-m3") == 0 &&
            (strcmp(argv[2], "-g") == 0 )&&argc==4){
        std::chrono::high_resolution_clock::time_point t1,t2;
        generator* generator1= new generator();
        std::string text = generator1->generate(atoi(argv[3]));
        std::cout<<"input: "<<text<<std::endl;
        SystematicSearch* search = new SystematicSearch(text);
        t1 = std::chrono::high_resolution_clock::now();
        search->sort();
        t2 = std::chrono::high_resolution_clock::now();
        std::cout<<"Time: " <<(double)(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000;
    } else if(strcmp(argv[1], "-m3") == 0 &&
              (strcmp(argv[2], "-f") == 0 )&& argc==4){
        std::string text;
        fileReader *input = new fileReader(argv[3]);
        text = input->readFile();
        std::cout<<"input: "<<text<<std::endl;
        SystematicSearch* search = new SystematicSearch(text);
        search->sort();
    }
    else if(strcmp(argv[1], "-m4") == 0 &&
            (strcmp(argv[2], "-g") == 0 )&&argc==4){
        //generator* generator1= new generator();
       //std::string text = generator1->generate(atoi(argv[3]));
        //std::cout<<"input: "<<text<<std::endl;
        SubstringSearch* search = new SubstringSearch("ppppcmykcmycmykppcmykcmy");
        std::pair<int,int> pair;
        try {
            pair = search->findLongestSubstring('c');
        }
        catch (NoLettersLeft n){
            std::cout<<" index: "<<pair.first<<" length: "<<pair.second<<std::endl;
        }
        std::cout<<" index: "<<pair.first<<" length: "<<pair.second<<std::endl;

    }
    else{
        std::cout<<" Invalid input\n";
    }
}


#endif //CMYKSORT_CMDHANDLER_H
