//
// Created by luke on 18.05.18.
//

#include <iostream>
#include "StringSearch.h"

void StringSearch::deleteFours() {
    unsigned long lengthLeft = tekst.size() - numberOfSorted;
    int numberOfFours = lengthLeft / 4;
    if (lengthLeft%4 == 0 )
        numberOfFours -=1;
    for (int i = 0; i < numberOfFours; ++i)
        move(numberOfSorted);
}

void StringSearch::deleteFours(int startingPos) {
    int lengthLeft = startingPos -numberOfSorted;
    int numberOfFours = 0;
    while (lengthLeft != 0){
        numberOfFours++;
        lengthLeft-=4;
    }

    for (int i = 0; i < numberOfFours; ++i) {
        move(numberOfSorted);
    }
}


void StringSearch::setLetter(char c) {
    unsigned long posNumber = tekst.find(c,numberOfSorted);
    if(posNumber > tekst.size())
        throw NoLettersLeft();
    unsigned long lengthLeft = tekst.size() - numberOfSorted;
    switch (lengthLeft%4){
        case 0:
            setLetter0(posNumber);
            break;
        case 1:
            setLetter1(posNumber);
            break;
        case 2:
            setLetter2(posNumber);
            break;
        case 3:
            setLetter3(posNumber);
            break;
    }
    numberOfSorted++;
}

void StringSearch::setLetter0(int pos) {
    unsigned long posNumberRelative = countRelativeIndex(pos);
    unsigned long distanceFromEnd = tekst.size() - pos;
    switch (posNumberRelative%4){
        case 0:
            deleteFours(pos);
            break;
        case 1:
            if(distanceFromEnd == 3 ){
                move(pos-5);
                move(tekst.size()-7);
            } else
                move(pos);
            deleteFours();
            break;
        case 2:
            if(distanceFromEnd == 2){
                move(pos-4);
                move(tekst.size()-6);
            } else
                move(pos);
            deleteFours();
            break;
        case 3:
            if(distanceFromEnd == 1){
                move(tekst.size()-5);
                move(tekst.size()-5);
            } else
                move(pos);
            deleteFours();
            break;
    }
}

void StringSearch::setLetter1(int pos) {
    unsigned long posNumberRelative = countRelativeIndex(pos);
    unsigned long distanceFromBeginning = pos - numberOfSorted;
    switch (posNumberRelative%4){
        case 0:
            deleteFours(pos);
            break;
        case 1:
            if(distanceFromBeginning == 1) {
               move(pos);
               move(tekst.size() - 7);
            } else
                move(pos - 3);
            deleteFours();
            break;
        case 2:
            if(distanceFromBeginning == 2){
                move(pos - 1);
                move(tekst.size() - 6);
            } else
                move(pos -3);
            deleteFours();
            break;
        case 3:
            move(pos - 3);
            deleteFours();
            break;
    }
}

void StringSearch::setLetter2(int pos) {
    unsigned long posNumberRelative = countRelativeIndex(pos);
    unsigned long distanceFromBeginning = pos - numberOfSorted;
    switch (posNumberRelative%4){
        case 0:
            deleteFours(pos);
            break;
        case 1:
            if(distanceFromBeginning == 1){
                move(pos -1);
                move(tekst.size() - 5);
            } else
                move(pos -2);
            deleteFours();
            break;
        case 2:
        case 3:
            move(pos-2);
            deleteFours();
            break;
    }
}

void StringSearch::setLetter3(int pos) {
    unsigned long posNumberRelative = countRelativeIndex(pos);
    unsigned long distanceFromEnd = tekst.size() - pos;
    switch (posNumberRelative%4){
        case 0:
            deleteFours(pos);
            break;
        case 1:
            if(distanceFromEnd == 2){
                move(pos-4);
                move(tekst.size()-7);
            } else
                move(pos-1);
            deleteFours();
            break;
        case 2:
            if(distanceFromEnd == 1){
                move(tekst.size()-5);
                move(tekst.size()-6);
            } else
                move(pos-1);
            deleteFours();
            break;
        case 3:
            move(pos-1);
            deleteFours();
            break;
    }
}


void StringSearch::sort() {
    char currentLetter [4] = {'c','m','y','k'};
    int index = 0;
    do{
        try {
            setLetter(currentLetter[index]);
        }catch (NoLettersLeft noLettersLeft){ break; }
        index = (index+1)%4;
    }while (tekst.size()-numberOfSorted>4);
}

void StringSearch::sortWithSystematicSearch() {
    char currentLetter [4] = {'c','m','y','k'};
    int index = 0;
    do{
        try {
            setLetter(currentLetter[index]);
        }catch (NoLettersLeft noLettersLeft){ break; }
        index = (index+1)%4;
    }while (tekst.size()-numberOfSorted>4);
    std::string temp = tekst.substr(tekst.size()-8,tekst.size()-1);
    tekst.erase(tekst.size()-8,tekst.size()-1);
    SystematicSearch* search = new SystematicSearch(temp);
    search->sort();
    temp = search->returnLast();
    tekst.append(temp);
}


void StringSearch::move(int pos) {
    std::string temp = tekst.substr(pos, 4);
    tekst.erase(pos, 4);
    tekst.append(temp);
}


std::ostream &operator<<(std::ostream &os, const StringSearch &grupa1) {
    os << "tekst: " << grupa1.tekst;
    return os;
}

StringSearch::StringSearch(const std::string &tekst) : tekst(tekst),numberOfSorted(0){}

int StringSearch::countRelativeIndex(int index) {
    return index-numberOfSorted;
}

StringSearch::~StringSearch() = default;











